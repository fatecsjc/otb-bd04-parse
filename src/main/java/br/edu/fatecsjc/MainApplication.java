package br.edu.fatecsjc;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class MainApplication {

    public static void main(String[] args) throws SQLException {

        List<String> classForNames = new ArrayList<String>(Arrays.asList("oracle.jdbc.driver.OracleDriver", "com.mysql.jdbc.Driver"));

        try {
            for (String classForname : classForNames) {
                Class.forName(classForname);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        List<String> urlConnetion = new ArrayList<String>(Arrays.asList("jdbc:oracle:thin:@localhost:1521:ora12cdb01", "jdbc:mysql://localhost:3306/obbd?autoreconnect&useSSL=false"));
        Connection connection1 = DriverManager.getConnection(urlConnetion.get(0), "hr", "aluno");
        Connection connection2 = DriverManager.getConnection(urlConnetion.get(1), "hr", "aluno");

        List<Connection> connections = new ArrayList<Connection>(Arrays.asList(connection1, connection2));
        testConnections(connections);

    }

    public static void testConnections(List<Connection> connections) throws SQLException {

        List<Long> times = new ArrayList<Long>();

        for (Connection connection : connections) {
            long startTime = System.currentTimeMillis();

            try {
                PreparedStatement ps = connection.prepareStatement("SELECT COUNT(*) FROM hr.tabela_controle2");
                ResultSet rs = ps.executeQuery();

                int id = 0;
                while (rs.next()) {

                    id = rs.getInt("COUNT(*");
                }

                ps.close();
                rs.close();

                long stopTime = System.currentTimeMillis();
                long timeExec = stopTime - startTime;

                times.add(timeExec);

            } catch (SQLException connException) {
                // connException.printStackTrace();
                System.err.println(connException.getMessage());
                System.err.println(connException.getErrorCode());
                System.err.println(connException.getSQLState());
            }


        }

        for (Long time : times) {
            System.out.println("Tempo decorrido: " + new SimpleDateFormat("mm:ss:SSS").format(new Date(time)));
        }
    }

    private static void selectAll(Connection connection) throws SQLException {

        try {

            PreparedStatement ps1 = connection.prepareStatement("SELECT valor_id, tc2_descricao FROM hr.tabela_controle2");
            ResultSet rs1 = ps1.executeQuery();


            while (rs1.next()) {

                int id = rs1.getInt("valor_id");
                String descricao = rs1.getString("tc2_descricao");
                System.out.println("Valor id: " + id + "\tDescrição: " + descricao);
                // valor_id += 1;
            }

            rs1.close();
            ps1.close();

        } catch (SQLException e) {
            System.err.println("==> SQLException: ");
            while (e != null) {
                System.out.println("Message: " + e.getMessage());
                System.out.println("SQLState: " + e.getSQLState());
                System.out.println("ErrorCode: " + e.getErrorCode());
            }
        }
    }
}
